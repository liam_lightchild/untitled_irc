module Untitled
  class Listener
    def handle_user(listener, client, arg)
      client.puts 'hi'
    end

    def handle_join(listener, client, arg)
      listener.reply client, ":#{listener.users[client.addr[3]][:username]} JOIN #{arg}"
      puts "-- #{listener.getstr client} joined #{arg}"
      listener.add_user_to_channel arg, client
    end

    def handle_nick(listener, client, arg)
      listener.reply client, ":#{listener.users[client.addr[3]][:username]} NICK #{arg}"
      listener.users[client.addr[3]][:username] = arg
    end

    def handle_privmsg(listener, client, arg)
      case arg
      when /#([a-zA-Z0-9]+) :(.+)/
        channel = "##{$1}"
        message = $2
        if channel == '#trigger' and message.start_with? '@'
          sub = '~'
          args = ''
          sub = message.split(':')[1] if message.include? ':'
          args = message.split('::(')[1].split(')')[0] if message.match? /::\(.*\)$/
          success = listener.commander.trigger message, args, sub, Untitled::Messenger.new(client)
          listener.reply client, ":Server PRIVMSG #{channel} :#{success ? "\x02\x033Successfully triggered":"\x02\x034Failed to trigger"} #{message.split('::(')[0]}"
        else
          puts "-- #{listener.getstr(client)} sent '#{message}' to channel #{channel}"
          listener.channels[channel] = {
              clients: [client],
              topic: ''
          } if listener.channels[channel].nil?
          listener.channels[channel][:clients].each do |c|
            next if c.addr[3] == client.addr[3]
            listener.reply c, ":#{listener.users[client.addr[3]][:username]} PRIVMSG #{channel} :#{message}"
          end
        end
      when /([a-zA-Z0-9]+) :(.+)/
        usern = $1
        message = $2
        puts "-- #{listener.getstr(client)} sent '#{message}' to user #{usern}"
        listener.users.each do |_, user|
          if user[:username] == usern
            user[:client].puts ":#{listener.users[client.addr[3]][:username]} PRIVMSG #{usern} :#{message}"
            return
          end
        end
      else
        listener.reply client, ":Server PRIVMSG #{listener.users[client.addr[3]][:username]} :\x02Invalid IRC Message."
      end
    end

    def handle_part(listener, client, arg)
      a = listener.channels
      a[arg.split(' ')[0]][:clients] -= [client.addr[3]]
      listener.channels = a
      listener.reply client, ":#{listener.users[client.addr[3]][:username]} PART #{arg.split(' ')[0]}"
      listener.broadcast ":Server PRIVMSG #{arg.split(' ')[0]} :#{listener.users[client.addr[3]][:username]} left", condition: ["in #{arg.split(' ')[0]}"]
    end

    def handle_list(listener, client, arg)
      client.puts ":small-cat 321 Channel :Users  Name"
      listener.channels.each do |channel|
        client.puts ":small-cat 322 - #{channel[0]} #{channel[1][:clients].length} :#{channel[1][:topic]}"
        p channel
      end
      client.puts ":small-cat 323 :End of /LIST"
    end
  end
end
