require 'untitled/rustleapi'

MANASLU_IP = '10.0.1.99'

extension do |server, meta|
  meta[:name] = 'Manaslu Server API'
  meta[:description] = <<~DESCRIPTION
    This extension allows to do some tasks on 10.0.1.99
  DESCRIPTION
  meta[:version] = '1.0.0'
  meta[:author] = 'Liam Cole'
  meta[:tags] = %w<triggers server api official>
  meta[:additions] = {
      triggers: %w<@blazing-inferno/restart @blazing-inferno/status>,
      commands: %w<>
  }

  server.register_trigger '@blazing-inferno/restart', lambda { |_, _| Rustle.exec_run MANASLU_IP, 'blazing/restart.rb' }
end
