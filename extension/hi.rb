extension do |server, meta|
  meta[:name] = 'the hello'
  meta[:description] = <<~DESCRIPTION
    This extension adds the @hi trigger.
    Thats it.
  DESCRIPTION
  meta[:version] = 1
  meta[:author] = 'Liam Cole'
  meta[:tags] = %w<triggers simple official>
  meta[:additions] = {
      triggers: %w<@hi>,
      commands: %w<>
  }

  server.register_trigger '@hi', lambda { |_, c| c.send_message '#trigger', 'Server', 'Hello!' }
end
